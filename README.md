# Readme

This is a small application/package written in Go, which translates MAC-adresses from one format to another.  
The following 3 formats are currently available:  
xxxx.xxxx.xxxx  
xx:xx:xx:xx:xx:xx  
xxxx-xxxx-xxxx  

Included is also an AutoHotKey script so you can run the program by using keybindings.  
This AHK script will look for the file **mactranslate.exe** within the current directory, so make sure to compile the Go package.  
By default, the keybindings are as follows:  
**CTRL+Shift+1** = xxxx.xxxx.xxxx  
**CTRL+Shift+2** = xx:xx:xx:xx:xx:xx  
**CTRL+Shift+3** = xxxx-xxxx-xxxx  


# Run & Compile
You can run the package directly with: **go run mactranslate.go**  
Or you can compile the package with: **go install mactranslate.go** OR **go build mactranslate.go**  
