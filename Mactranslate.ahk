#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
#SingleInstance force
SetTitleMatchMode, 2

; Mactranslate Cisco, xxxx.xxxx.xxxx
+^1:: ;CTRL+Shift+1
; Run "D:\repositories\Projects\go\mactranslate\mactranslate.exe" --cisco, ,Hide
Run "mactranslate.exe" --cisco, ,Hide
Return

; Mactranslate ESS, xx:xx:xx:xx:xx:xx
+^2:: ;CTRL+Shift+2
; Run "D:\repositories\Projects\go\mactranslate\mactranslate.exe" --ess, ,Hide
Run "mactranslate.exe" --ess, ,Hide
Return

; Mactranslate Huawei, xxxx-xxxx-xxxx
+^3:: ;CTRL+Shift+3
; Run "D:\repositories\Projects\go\mactranslate\mactranslate.exe" --huawei, ,Hide
Run "mactranslate.exe" --huawei, ,Hide
Return
